#!/bin/bash -x

TAG=$(git tag -l $1)

if [[ "$TAG" == '' ]]; then

     git tag $1 && git push origin $1

else 

     git push --delete origin $1 && git tag --delete $1  && git tag $1 && git push origin $1

fi

