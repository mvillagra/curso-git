#!/bin/bash 

PASS=$1

container=$(docker ps -aq --filter "name=sur")

echo $container 

if [[ "$container" == '' ]]; then          

       docker run -d -p 8092:80 --name sur sur.0.0.$PASS

else 

       docker stop $container && docker rm $container && docker run -d -p 8092:80 --name sur sur.0.0.$PASS

fi
