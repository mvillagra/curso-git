
https://medium.com/google-cloud/kubernetes-configmaps-and-secrets-68d061f7ab5b

--

docker login

docker build -t mativillagra/imagenes:0.1 -f Dockerfile  .

docker push mativillagra/imagenes:0.1


--


docker pull mativillagra/imagenes:0.1

docker run -d -e LANGUAGE=guarani -e API_KEY=1403 -p 8082:3000 -ti mativillagra/imagenes:0.1



--


kubectl create secret generic secreto --from-literal=API_KEY=Bryan -n default

kubectl create configmap language --from-literal=LANGUAGE=English -n default

--


Kubernetes solves this problem with Secrets (for confidential data) and ConfigMaps (for non-confidential data).

The big difference between Secrets and ConfigMaps are that Secrets are obfuscated with a Base64 encoding. There may be more differences in the future, but it is good practice to use Secrets for confidential data (like API keys) and ConfigMaps for non-confidential data (like port numbers).


--

kubectl create secret docker-registry node --docker-server=registry.gitlab.com --docker-username=gitlab+deploy-token-23518 --docker-password=zWLYKg9TFBjz944aQsBU --docker-email=kubernetes@dgrc.gov.ar -n defaul


-- 


kubectl create -f map.yaml

apiVersion: v1
kind: ConfigMap
metadata:
 name: test
 namespace: default
data:
 LANGUAGE: Chinese
