#!/bin/bash 

PASS=$1

container=$(docker ps -aq --filter "name=oeste")

echo $container 

if [[ "$container" == '' ]]; then          

       docker run -d -p 8094:80 --name oeste oeste.0.0.$PASS

else 

       docker stop $container && docker rm $container && docker run -d -p 8094:80 --name oeste oeste.0.0.$PASS

fi
